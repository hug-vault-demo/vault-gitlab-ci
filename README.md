# vault-gitlab-ci

Demo project for managing Hashicorp Vault configuration with Gitlab and Terraform

## CI

For security considerations during merge requests, this project is configured to use an [external CI configuration](https://gitlab.com/hug-vault-demo/vault-gitlab-terraform-ci-config/-/raw/main/vault-gitlab-ci.yml)

## Bootstrap

```
# use 0.15.0 or <= whatever the gitlab wrapper stable version is
tfenv install 0.15.0
tfenv use 0.15.0

export STATE_NAME="gitlab-vault-ci"
export PROJECT_ID="24757194"

export "TF_HTTP_ADDRESS=https://gitlab.com/api/v4/projects/${PROJECT_ID}/terraform/state/${STATE_NAME}"
export "TF_HTTP_LOCK_ADDRESS=https://gitlab.com/api/v4/projects/${PROJECT_ID}/terraform/state/${STATE_NAME}/lock"
export "TF_HTTP_LOCK_METHOD=POST"
export "TF_HTTP_UNLOCK_ADDRESS=https://gitlab.com/api/v4/projects/${PROJECT_ID}/terraform/state/${STATE_NAME}/lock"
export "TF_HTTP_UNLOCK_METHOD=DELETE"
export "TF_HTTP_RETRY_WAIT_MIN=5"

export TF_CLI_ARGS_init="-backend-config='username=username' -backend-config='password=yourapikey'"

terraform init
terraform plan
terraform apply
```
