## Auth backend definitions

## JWT with gitlab.it.ufl.edu
resource "vault_jwt_auth_backend" "jwt" {
  description  = "Configuration for the JWT backend against gitlab.com"
  path         = "gitlab"
  jwks_url     = "https://gitlab.com/-/jwks"
  bound_issuer = "gitlab.com"
}

## AppRole
resource "vault_auth_backend" "approle" {
  type = "approle"
}

## Azure
resource "vault_auth_backend" "azure" {
  type = "azure"
}

resource "vault_azure_auth_backend_config" "azure" {
  backend       = "azure"
  tenant_id     = "019f002d-0ea0-4d2e-89d5-675589e01c5b"
  resource      = "https://vault.az.oulman.dev"
  client_id     = "f3097425-d7b3-44f4-9388-f8bd4b9ca051"
}
