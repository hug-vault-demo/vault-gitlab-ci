resource "vault_mount" "secrets" {
  path        = "secrets"
  type        = "kv-v2"
}
