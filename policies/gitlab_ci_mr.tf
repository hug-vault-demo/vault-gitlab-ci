resource "vault_policy" "gitlab_ci_mr" {
  name = "gitlab_ci_mr"

  policy = file("policies/files/gitlab_ci_mr.hcl")
}
