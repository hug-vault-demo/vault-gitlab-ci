resource "vault_policy" "vault-demo_read" {
  name = "vault-demo_read"

  policy = file("policies/files/vault-demo_read.hcl")
}
