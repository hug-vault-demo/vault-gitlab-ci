path "sys/auth"
{
  capabilities = ["read"]
}

path "auth"
{
  capabilities = ["read"]
}

path "auth/*"
{
  capabilities = ["list", "read"]
}

path "auth/token/create" {
  capabilities = ["create", "read", "update", "list"]
}

# List
path "sys/auth/*"
{
  capabilities = ["list"]
}

# List existing policies
path "sys/policy"
{
  capabilities = ["list"]
}

# Create and manage ACL policies broadly across Vault
#path "sys/policy/*"
#{
#  capabilities = ["create", "read", "update", "delete", "list", "sudo"]
#}

# List existing policies
path "sys/policies"
{
  capabilities = ["list"]
}

# Create and manage ACL policies broadly across Vault
path "sys/policies/*"
{
  capabilities = ["list","read"]
}

# List, create, update, and delete key/value secrets
#path "secret/*"
#{
#  capabilities = ["create", "read", "update", "delete", "list", "sudo"]
#}

# Read mounts
path "sys/mounts"
{
  capabilities = ["read"]
}

path "sys/mounts/*"
{
  capabilities = ["list", "read"]
}

path "azure"
{
  capabilities = ["read"]
}

path "azure/roles/*"
{
  capabilities = ["list", "read"]
}

path "ssh-client-signer"
{
  capabilities = ["read"]
}

path "ssh-host-signer"
{
  capabilities = ["read"]
}

path "ssh-host-signer/*"
{
  capabilities = ["list", "read"]
}

path "ssh-client-signer/*"
{
  capabilities = ["list", "read"]
}
