path "sys/auth"
{
  capabilities = ["read"]
}

path "auth"
{
  capabilities = ["read"]
}

path "auth/*"
{
  capabilities = ["create", "read", "update", "delete", "list", "sudo"]
}

# List, create, update, and delete auth backends
path "sys/auth/*"
{
  capabilities = ["create", "read", "update", "delete", "sudo"]
}

# List existing policies
path "sys/policy"
{
  capabilities = ["read"]
}

# Create and manage ACL policies broadly across Vault
path "sys/policy/*"
{
  capabilities = ["create", "read", "update", "delete", "list", "sudo"]
}

# List existing policies
path "sys/policies"
{
  capabilities = ["read"]
}

# Create and manage ACL policies broadly across Vault
path "sys/policies/*"
{
  capabilities = ["create", "read", "update", "delete", "list", "sudo"]
}

# List, create, update, and delete key/value secrets
path "secret/*"
{
  capabilities = ["create", "read", "update", "delete", "list", "sudo"]
}

# Read mounts
path "sys/mounts"
{
  capabilities = ["read"]
}

# Manage and manage secret backends broadly across Vault.
path "sys/mounts/*"
{
  capabilities = ["create", "read", "update", "delete", "list", "sudo"]
}

# Read health checks
path "sys/health"
{
  capabilities = ["read", "sudo"]
}

path "ad"
{
  capabilities = ["read"]
}

path "ad/*"
{
  capabilities = ["create", "read", "update", "delete", "list", "sudo"]
}

path "gcp"
{
  capabilities = ["read"]
}

path "gcp/roleset/*"
{
  capabilities = ["create", "read", "update", "delete", "list", "sudo"]
}

path "azure"
{
  capabilities = ["read"]
}

path "azure/roles/*"
{
  capabilities = ["create", "read", "update", "delete", "list", "sudo"]
}

path "ssh-client-signer"
{
  capabilities = ["read"]
}

path "ssh-host-signer"
{
  capabilities = ["read"]
}

path "ssh-host-signer/*"
{
  capabilities = ["create", "read", "update", "delete", "list", "sudo"]
}

path "ssh-client-signer/*"
{
  capabilities = ["create", "read", "update", "delete", "list", "sudo"]
}
