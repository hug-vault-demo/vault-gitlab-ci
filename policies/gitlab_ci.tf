resource "vault_policy" "gitlab_ci" {
  name = "gitlab_ci"

  policy = file("policies/files/gitlab_ci.hcl")
}
