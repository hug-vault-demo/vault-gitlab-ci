resource "vault_azure_auth_backend_role" "demo-vm-reader" {
  backend                         = "azure"
  role                            = "demo-vm-reader"
  bound_subscription_ids          = ["2e444f0a-664d-4197-a548-fe56818248e4"]
  bound_service_principal_ids     = ["b3555673-b8ff-478c-8fd8-230123bac2f5"]
  token_ttl                       = 60
  token_max_ttl                   = 60
  token_policies                  = ["vault-demo_read"]
}
