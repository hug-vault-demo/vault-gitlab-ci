resource "vault_mount" "ssh-client-signer" {
  path = "ssh-client-signer"
  type = "ssh"
}

resource "vault_ssh_secret_backend_ca" "client" {
  backend              = vault_mount.ssh-client-signer.path
  generate_signing_key = "true"
}

resource "vault_mount" "ssh-host-signer" {
  path                  = "ssh-host-signer"
  type                  = "ssh"
}

resource "vault_ssh_secret_backend_ca" "host" {
  backend              = vault_mount.ssh-host-signer.path
  generate_signing_key = "true"
}

module "ssh" {
  source = "./ssh"
}
