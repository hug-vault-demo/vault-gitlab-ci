
resource "vault_jwt_auth_backend_role" "gitlab_ci" {
  backend        = "gitlab"
  role_name      = "gitlab_ci"
  token_policies = ["gitlab_ci"]

  bound_claims = {
    "project_id" : "24757194",
    "ref" : "main",
    "ref_type" : "branch",
    "ref_protected" : "true"
  }
  user_claim = "user_email"
  role_type  = "jwt"
}
