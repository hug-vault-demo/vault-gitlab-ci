
resource "vault_jwt_auth_backend_role" "gitlab_ci_mr" {
  backend        = "gitlab"
  role_name      = "gitlab_ci_mr"
  token_policies = ["gitlab_ci_mr"]

  bound_claims = {
    "project_id" : "24757194",
  }
  user_claim = "user_email"
  role_type  = "jwt"
}
