resource "vault_ssh_secret_backend_role" "hostrole" {
  name                    = "hostrole"
  backend                 = "ssh-host-signer"
  key_type                = "ca"
  allow_host_certificates = true
  allowed_domains         = "az.oulman.dev"
  allow_subdomains        = false
  ttl                     = "315360000"
}
