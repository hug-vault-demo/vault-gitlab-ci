resource "vault_ssh_secret_backend_role" "oulman" {
  name                    = "oulman"
  backend                 = "ssh-client-signer"
  key_type                = "ca"
  allow_user_certificates = true
  default_user            = "oulman"
  allowed_users           = "oulman, azureuser"
  ttl                     = "28800"
  max_ttl                 = "36000"
}
